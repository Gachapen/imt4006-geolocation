\section{Introduction}
The W3C Geolocation API is a web standard that allows scripts running in web browsers to get access to the geographic position of the device.
It is specified by the World Wide Web Consortium (W3C) and is currently a W3C Recommendation~\cite{Geolocation}, which in practice means that web browsers should implement it.

Describe uses...

The API is agnostic to the way the position information is sourced.
In practice this means that any way of finding the position of the device can be used.
Examples of common sources are GPS, WiFi, Bluetooth and cell towers.

\section{API description}

\subsection{Overview}
The Geolocation API is accessed through the Geolocation object.
This object is provided by the NavigatorGeolocation interface which is an extension of the Navigator interface.
In practice, this means the Geolocation API is accessed through the \lstinline{window.navigator.geolocation} object in an application.

The Geolocation object provides two methods of accessing the position.
The first method is to get the current position by using the \lstinline{getCurrentPosition()} function.
The second method is to watch the position changes using the \lstinline{watchPosition()} function.
They have similar interfaces, but act in different ways and should be used in different use cases.

All Geolocation functionality requires explicit permissions by the user.
The web browsers handle the permission request or revocation, so the application only needs to handle the case where permission is denied.

\subsection{Getting the position}
To get the current position of the device, \lstinline{getCurrentPosition()} should be used.
This function is an attribute of the Geolocation object and can thus be accessed through it.
The function interface is defined as follows:
\begin{lstlisting}
void getCurrentPosition(PositionCallback successCallback,
                        optional PositionErrorCallback errorCallback,
                        optional PositionOptions options);
\end{lstlisting}

The function will start an asynchronous request for the position and return immediately.
If and when the position is found, it will call the successCallback provided with the position as a parameter.
If it could not find the position, it will call the errorCallback function.

A simple case example of using this function is to find the closest store of some kind:
\begin{lstlisting}
function showClosestStore(position) {
    var store = stores.findClosest(position.coords.latitude,
                                   position.coords.longitude);
    showStore(store);
};

var geolocation = window.navigator.geolocation;
geolocation.getCurrentPosition(showClosestStore);
\end{lstlisting}

The successCallback takes the position as the first parameter.
This is an object of the Position interface, which provides two attributes: coords and timestamp.
The coords attribute is an object of the Coordinates interface and provides multiple attributes describing the position.
Most of these are optional and might not be provided by the implementation.
The three non-optional attributes are longitude, latitude and accuracy.
Longtitude and latitude describe the geographic position in the World Geodetic System (2d) (WGS84).
Accuracy indicates the accuracy level of the position in meters.
It is required to have a 95\% confidence level.

Building on the previous example, it can be extended to check the accuracy of the position before showing the store:
\begin{lstlisting}
function showClosestStore(position) {
    // Only show store if the position is accurate to 100 meters
    if (position.coords.accuracy < 100) {
        var store = stores.findClosest(position.coords.latitude,
                                       position.coords.longitude);
        showStore(store);
    }
};
\end{lstlisting}

\subsection{Other Position information}
The optional attributes describe the altitude, altitude accuracy (altitudeAccuracy), heading and speed.
These can be used in the same manner as latitude, longitude and accuracy.
Altitude is specified in meters above the WGS84 ellipsoid.
The altitude accuracy is the accuracy level of the altitude in meters and has a 95\% confidence interval.

Heading and speed together define the velocity of the device.
The speed is specified in meters per second.
If the speed is 0, it has no heading and thus the heading is NaN.
Otherwise, the heading is specified in degrees counting clockwise from the true north.
An example using all the attributes:
\begin{lstlisting}
function showPositionInfo(position) {
    var corrds = position.coords;

    showText("The device is located at " + coords.latitude + " " + coords.longitude");
    showText("The position is accurate to " + coords.accuracy + " meters");

    if (coords.altitude !== null) {
        showText("It has an altitude of " + coords.altitude + " meters");
        showText("The accuracy of the altitude is" + coords.altitudeAccuracy + " meters");
    } else {
        showText("Altitude information is not available");
    }

    if (coords.speed !== null) {
        showText("It is moving with a speed of " + coords.speed " meters per second");

	if (coords.speed !== 0 && coords.heading !== null) {
            showText("It is heading " + coords.heading + " degrees from the north");
        } else if (coords.heading === null) {
            showText("Heading information is not available");
        }
    } else {
        showText("Speed information is not available");
    }
};

var geolocation = window.navigator.geolocation;
geolocation.getCurrentPosition(showPositionInfo);
\end{lstlisting}

\subsection{Setting options}
The \lstinline{getCurrentPosition()} function can be configured by passing a PositionOptions object as the third argument.
This object has three different attributes: enableHighAccuracy, timout and maximumAge.
enableHighAccuracy is a boolean attribute and tells the position provider if it should try to get high accuracy info or not.
When this is set to true, the provider will try to get the best possible results.
To do this, it might use more battery consuming hardware, such as GPS, and use longer time.
It is important to note that this is a hint, the device might not be able to get any more accurate results than with this set to false.
By default it is set to false.

The timeout attribute defines how long the provider should try to get a position before giving up and reporting an error.
It is specified in milliseconds and is set to Infinity by default.

The maximumAge attribute can be set to allow cached positions to be provided.
By default the attribute is set to 0 and will in this case not use a cached position.
If set to a number larger than 0, it defines how old, in milliseconds, a cached position can be.
If the provider finds a cached position that is younger than maximumAge it will call the successCallback with this position rather than finding a new position.
If it can't find a cached position that is younger, it will try to find a new position.
The store example can be extended to set these options:
\begin{lstlisting}
var options = {
	enableHighAccuracy: false, // Not necessary for finding store
	timeout: 3000, // User probably gives up after 3 seconds
	maximumAge: 5 * 60 * 1000 // Probably didn't move much the last 5 minutes
};

var geolocation = window.navigator.geolocation;
geolocation.getCurrentPosition(showClosestStore, null, options);
\end{lstlisting}

\subsection{Handling errors}
The device can't always find a position, so it is important to handle any errors that occur.
To do this, the code can pass a function to the errorCallback parameter that will be called if the attempt fails.
Building on the previous store examples, the code could be improved to show an error message to the user and provide another option if it fails:
\begin{lstlisting}
function handlePositionError(error) {
    // Display error message to user
    // and provide another option (e.g. search)
};

var geolocation = window.navigator.geolocation;
geolocation.getCurrentPosition(showClosestStore,
                               handlePositionError,
                               options);
\end{lstlisting}

The error object passed to the errorCallback function provides more specific details about why it failed.
\begin{lstlisting}
interface PositionError {
    const unsigned short PERMISSION_DENIED = 1;
    const unsigned short POSITION_UNAVAILABLE = 2;
    const unsigned short TIMEOUT = 3;
    readonly attribute unsigned short code;
    readonly attribute DOMString message;
};
\end{lstlisting}

The code member of the error specifies what type of error occurred.
It has three possible values:
\begin{itemize}
	\item \lstinline{PERMISSION_DENIED}
	\item \lstinline{POSITION_UNAVAILABLE}
	\item \lstinline{TIMEOUT}
\end{itemize}

The standard requires browsers to ask for permission, so the browser will show a GUI dialogue to the user which asks if they will grant the permission to get their position.
If they deny, the \lstinline{PERMISSION_DENIED} error code is sent.
In this case, the application will not get any position information with any of the functions unless the user explicitly revokes the denial.

If the device can't determine the location, the \lstinline{POSITION_UNAVAILABLE} error code is sent.
The reason for the error can for example be that the internal location acquisition process generated an error, though this is not specified.

The last error, \lstinline{TIMEOUT}, happens when the device could not find a position before the length of time specified by the timeout option has elapsed.

In addition, the PositionError provides a message attribute which describes the error in a string.
This is not meant for actual application use, but can be used for debugging purposes.

The previous store examples can be extended to handle these error codes:
\begin{lstlisting}
function handlePositionError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            showMessage("Access to position was denied");
	    // Show some alternative method
	    break;
        case error.POSITION_UNAVAILABLE:
            showMessage("Could not get position");
	    // Show some alternative method
	    break;
        case error.TIMEOUT:
            // Keep trying in background.
	    options.timeout = Infinity;
            geolocation.getCurrentPosition(showClosestStore,
                                           handlePositionError,
                                           options);
            showMessage("Taking too long to get position");
	    // Show some alternative method
	    break;
    };
};

var geolocation = window.navigator.geolocation;
geolocation.getCurrentPosition(showClosestStore,
                               handlePositionError,
                               options);
\end{lstlisting}

\subsection{Watching the position}
While getting the current position can be useful in many cases, there are cases where you need to keep watching how the position changes.
In this case the \lstinline{watchPosition()} function is available.
It is similar to the \lstinline{getCurrentPosition()} function in that it has the same parameters and does mostly the same job.
What is different is that it keeps watching the position until the code clears the watch.
This section will describe the differences from \lstinline{getCurrentPosition()}, thus everything not described here can be assumed to be the same.

A common case for watching the position is a navigation application, such as Google Maps.
To start watching a position, the \lstinline{watchPosition()} function is called with the same parameters as \lstinline{getCurrentPosition()}.
This function starts an asynchronous operation to find the position, report it, and keep updating the position.
It will keep calling successCallback when the position significantly changes.
The function returns an integer that uniquely identifies the watch operation.
This identifier can be used to clear the watch operation by calling \lstinline{clearWatch()} with the identifier as the argument.
\begin{lstlisting}
function onPositionChanged(position) {
    // Update the dot indicating the position on the map
};

var geolocation = window.navigator.geolocation;
var watchId = geolocation.watchPosition(onPositionChanged);

function stopPositionWatching() {
    geolocation.clearWatch(watchId);
};
\end{lstlisting}

As with \lstinline{getCurrentPosition()}, \lstinline{watchPosition()} can fail with an error.
In this case though, the watch operation will keep trying, even when an error occurred.
Thus it is a good idea to handle these errors and in some cases clear the watch:
\begin{lstlisting}
function handlePositionError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            showMessage("Access to position was denied");
	    geolocation.clearWatch(watchId);
	    break;
        case error.POSITION_UNAVAILABLE:
            showMessage("Could not get position");
	    geolocation.clearWatch(watchId);
	    break;
        case error.TIMEOUT:
            showMessage("Finding the position is taking a long time...");
	    break;
    };
};

var geolocation = window.navigator.geolocation;
var watchId = geolocation.watchPosition(onPositionChanged
                                        handlePositionError);

function stopPositionWatching() {
    geolocation.clearWatch(watchId);
};
\end{lstlisting}

\section{Limitations and challenges}
The Geolocation API provides a simple interface with only two functions.
A website can either get the current position or watch the position, no more, and the method for acquiring the position is agnostic.
This makes for an interface that is simple to use and implement, but at the same time it adds some limitations.
To show these limitations we can compare the interface to that of Android~\cite{AndroidLocation}.

Using Android's native interface, an application can see all the different location providers, such as GPS or network.
The API also allows access to see each GPS satellite in used and query information about each of them.
The application can fine tune how frequent they want location updates to be received.
All this allows an application to more finely manage its location accuracy and battery consumption.
Compare this with the Geolocation API and the application only has the ability to enable or disable high accuracy and allowing cached positions.

A challenge is privacy.
The Geolocation API specification has explicit statements about the user's privacy.
The user must explicitly accept the use of their location.
If denied, it is impossible for the application to get the location without the user explicitly revoking the denial through the user agent (web browser).
This is handled by the web browser, so the implementation is not a challenge, but the application designer must take care to properly inform the user of why they want the location, otherwise it is likely that they will deny it.

The specification also states that the location data received must only be used for the task, it must be disposed after the task is done, it must be protected from unauthorized access and it can't be retransmitted.
An exception to all of these is when the user explicitly allows the application to do this.
The application is required to clearly disclose that it uses the data, how it uses the data, how long it stores the data, how it is secured and how it is shared.
It must also disclose how a user can update or delete the data.
All of this can make using the API challenging, especially if you plan to store or retransmit the data.
